import express from 'express';
import session from 'express-session';

import testQustions from './tech-test-sample.json';

// Set up the express app
const app = express();
app.use(session({secret: 'secretMessage', saveUninitialized: true, resave: true}));
app.use(express.json());

let currentSession;

const WEIGHT_THRESHOLD = 1000;

/**
 * Create new session
 */
app.get('/create-session', (req, res) => {
  currentSession = req.session;
  currentSession.exists = true;
  currentSession.questions = testQustions.questions;
  currentSession.answers = {};
  res.status(200).send({
    message: 'Session created'
  })
});

/**
 * Get next unanswered question
 */
app.get('/next-question', (req, res) => {
  currentSession = req.session;
  if (currentSession.exists) {
    let nextQuestion = getNextQuestion(currentSession)
    currentSession.currentQuestion = nextQuestion;
    res.status(200).send({
      question: nextQuestion
    })
  } else {
    res.status(401).send({
      message: 'No session created'
    })
  }
});

/**
 * Submit an answer to the current question
 * Request body params: answer
 */
app.post('/submit-answer', (req, res) => {
  currentSession = req.session;
  if (currentSession.exists) {
    let question = currentSession.currentQuestion
    let weightWithinThreshold = handleQuestion(question, currentSession, req.body.answer, question.id);

    if (weightWithinThreshold) {
      res.status(200).send({
        success: true
      })
    } else {
      res.status(400).send({
        success: false,
        message: "Weight exceeded"
      })
    }
  } else {
    res.status(401).send({
      message: 'No session created'
    })
  }
});

/**
 * Change an answer to a specified question based on the question ID
 * Request body params: id, answer
 */
app.post('/change-answer', (req, res) => {
  currentSession = req.session;
  if (currentSession.exists) {
    let question = Object.values(currentSession.questions).find(item => item.id === req.body.id) //retrieve the question based on id
    let weightWithinThreshold = handleQuestion(question, currentSession, req.body.answer, question.id);

    if (weightWithinThreshold) {
      res.status(200).send({
        success: true
      })
    } else {
      res.status(400).send({
        success: false,
        message: "Weight exceeded"
      })
    }
  } else {
    res.status(401).send({
      message: 'No session created'
    })
  }
});

/**
 * Helper function for handling different types of questions
 */
function handleQuestion(question, currentSession, answer, id) {
  switch (question.type) {
    case 'datePicker':
      if (answer < question.minYears) {
        return false
      }
      currentSession.answers[id] = answer
      break;
    case 'table':
      let foundAnswer = question.answerOptions.find(item => item.id === answer);
      if (foundAnswer.weight >= WEIGHT_THRESHOLD) {
        return false;
      }
      currentSession.answers[id] = answer
      break;
    case 'tableMulti':
      let total = 0;
      question.answerOptions.forEach(item => {
        total += (answer.includes(item.id)) ? item.weight : 0;
      })
      if (total >= WEIGHT_THRESHOLD) {
        return false;
      }
      currentSession.answers[id] = answer
      break;
    default:
      currentSession.answers[id] = answer
  }
  return true;
}

/**
 * Get next unanswered question handling all dependencies
 */
function getNextQuestion(currentSession) {
  return Object.values(currentSession.questions).find(item => {
    let answerSubmitted = !(item.id in currentSession.answers)
    if (item.dependencies) {
      for (const dependency of item.dependencies) {
        switch (dependency.type) {
          case 'age':
            let currentAge = getAge(currentSession.answers.age)
            return dependency.allowedRanges.find(i => currentAge >= i.minYears && currentAge <= i.maxYears) !== undefined;
        }
      }
    } else {
      return answerSubmitted;
    }
  })
}

/**
 * Convert DOB to age
 * @param {*} DOB 
 */
function getAge(DOB) {
  var today = new Date();
  var birthDate = new Date(DOB);
  var age = today.getFullYear() - birthDate.getFullYear();
  var m = today.getMonth() - birthDate.getMonth();
  if (m < 0 || (m === 0 && today.getDate() < birthDate.getDate())) {
      age = age - 1;
  }

  return age;
}


const PORT = 5000;

app.listen(PORT, () => {
  console.log(`server running on port ${PORT}`)
});